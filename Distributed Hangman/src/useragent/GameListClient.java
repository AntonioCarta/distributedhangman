package useragent;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;

import common.HangmanGameListItem;
import common.IGameList;

/* Gestisce la lista partite lato client. Acceduto tramite RMI dalla registry. */
public class GameListClient extends UnicastRemoteObject implements IGameList {
	private static final long serialVersionUID = -475954678250395442L;
	
	private JSONParser parser;
	private List<HangmanGameListItem> gamelist;
	
	protected GameListClient() throws RemoteException {
		parser = new JSONParser();
	}

	/* Inizializza la lista con i dati iniziali. */
	public void initList(String jsongames) throws RemoteException {
		JSONArray l = (JSONArray) JSONValue.parse(jsongames);
		gamelist = new ArrayList<>(l.size());
		for(int i=0; i<l.size(); i++) {
			gamelist.add(i, new HangmanGameListItem((JSONObject) l.get(i)));
		}
	}

	/* Aggiorna un gioco. Usato dalla registry per inviare gli aggiornamenti. */
	public synchronized void upDateGame(String jsons) {
		if(gamelist != null) {
			JSONObject jo;
			try {
				jo = (JSONObject) parser.parse(jsons);
				HangmanGameListItem g = new HangmanGameListItem(jo);
				gamelist.set(g.gameid, g);
			} catch (ParseException e) {
				// Non riesco a parsare il messaggio ricevuto. Lo scarto.
			} catch (Exception e) {
				//il messaggio � corrotto. Viene ignorato.
			}		
		}
	}
	
	public synchronized void printList() {
		if(gamelist == null) {
			System.out.println("In attesa della lista delle partite dalla registry...");
			return;
		}
		
		for(HangmanGameListItem g : gamelist) 
			System.out.println(g.prettyString());
	}
	
	/// return null se l'indice non � valido.
	public synchronized HangmanGameListItem getGame(int index) {
		if(index>=0 && index<gamelist.size())
			return gamelist.get(index);
		return null;
	}
	
	public boolean isEmpty() { return gamelist == null;}
}
