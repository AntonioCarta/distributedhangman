package registry;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import common.ConnectionInfo;
import common.HangmanGameListItem;

public class GameServer extends Thread{
	GameInfo[] games;
	int serverPort; 
	Executor myPool = Executors.newCachedThreadPool();
		
	//inizializza l'array delle partite disponibili con partite vuote
	public GameServer(int gamenum, int serverPort, GameListServer gls) {
		this.serverPort = serverPort;
		
		games = new GameInfo[gamenum]; 
		for(int i = 0; i<games.length; i++) {
			//la password non serve per sicurezza quindi va bene anche un'inizializzazione banale	
			ConnectionInfo conn = new ConnectionInfo("225.42.42." + i, 4242 + i, "password" + i);
			games[i] = new GameInfo(conn, i, gls);
		}
	}
	
	//resta in ascolto di nuovi giocatori che desiderano connettersi alle partite
	@Override
	public void run() {
		//con questo costrutto la chiusura del socket � automatica all'uscita dal blocco.
		try (ServerSocket so = new ServerSocket(serverPort)) {
			while(true) {				
				acceptClient(so.accept());
			}
		} catch (IOException e) {
			System.out.println("errore di connessione nel server partite.");
			e.printStackTrace();
		}
	}

	///si occupa dell'inizializzazione della partita gestendo la connessione TCP
	///con tutti i partecipanti. Nel caso del master la connessione resta aperta fino
	///alla fine della partita, per gli altri invece viene chiusa al termine dell'inizializzazione.
	private void acceptClient(final Socket accept) {
		Thread t = (new Thread(){
			int playerid = -1;
			Random rand = ThreadLocalRandom.current();
			
			@Override
			public void run() {
				GameInfo g = null;				
				boolean isMaster = false;
				
				try {										
					DataInputStream inp = new DataInputStream(accept.getInputStream());
					int gameid = inp.readInt();																			
					
					if(gameid<games.length)
						g = games[gameid];
					else {
						//id gioco non valido.
						try {
							accept.close();
						} catch(IOException e) {}
						return;
					}
					
					//Partita in corso, non posso unirmi.
					if(g.getStatus() == HangmanGameListItem.GAME_PLAYING) {
						try {
							accept.close();
						} catch(IOException e) {}
						return;
					}
					
					OutputStream out = accept.getOutputStream();									
					isMaster = g.setMaster(accept);
					//informo il giocatore se � un master o no
					out.write(isMaster ? 1 : 0);
					if(isMaster)
						connectMaster(inp, g);
					else 
						connectGuesser(inp, g);
					
				} catch (IOException e) {
					//Se il giocatore � un master, annullo la partita.
					if(isMaster) 
						g.closeGame();					
					//Il gioco � iniziato. La chiusura � stata corretta.
					if(g == null || g.getStatus() == HangmanGameListItem.GAME_PLAYING){
						return;
					}
					//Errore durante la connessione (o abbandono del giocatore). Elimino il giocatore dalla partita.
					if(playerid != -1)
						g.removePlayer(playerid);
				} finally {
					//chiudo il socket
					try {
						accept.close();
					} catch (IOException e1) { }
				}
				
			}

			private void connectGuesser(DataInputStream inp, GameInfo g) throws IOException {
				if(g.getStatus() == HangmanGameListItem.GAME_INITIALIZATION) 
					playerid = g.addPlayer(accept);											
				else //La partita � ancora in corso. Annullare la connessione.
					return;
				
				//Attendi chiusura. Fallisce se � un guesser e la partita � iniziata.
				//Se ricevo qualcosa vuol dire che un guesser � convinto di essere master.
				//In questo caso lo elimino dalla partita.
				char c = inp.readChar();
				
				//Se arrivo qui allora devo eliminare il guesser dalla partita.
				g.removePlayer(playerid);
			}

			private void connectMaster(DataInputStream inp, GameInfo g) throws IOException {
				g.conn.password = String.valueOf( rand.nextDouble() );
				int maxp = inp.readInt();
				g.setNewGame(maxp, inp.readUTF()); //inizializzo la partita con num. giocatori e nome master								
				
				//Attendo la chiusura della partita dal master.
				char c = inp.readChar();
									
				//devo chiudere tutta la partita
				g.closeGame();
			};
		});
		myPool.execute(t);
	}
}
