package common;
import org.json.simple.JSONObject;


public class ConnectionInfo {
	public String address;
	public int port;
	public String password;
	
	public ConnectionInfo(String addr, int port, String pass) {
		this.address = addr;
		this.port = port;
		this.password = pass;
	}
	
	public ConnectionInfo(JSONObject jo) {
		this.address = (String) jo.get("address");
		this.port = ((Number)jo.get("port")).intValue();
		this.password = (String) jo.get("password");
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject jo = new JSONObject();
		jo.put("address", address);
		jo.put("port", port);
		jo.put("password", password);
		return jo;
	}

	/* Verifica se il JSONObject passato � una rappresentazione corretta di un oggetto ConnectionInfo */
	@SuppressWarnings("unused")
	public static boolean validateJSON(JSONObject jo) {
		if(jo==null) return false;
		
		if(!jo.containsKey("address") || !jo.containsKey("port") || !jo.containsKey("password"))
			return false;
		try {
			String address = (String) jo.get("address");
			int port = ((Number)jo.get("port")).intValue();
			String password = (String) jo.get("password");
		} catch(Exception e) {
			//Non riesco a castare. Tipi errati.
			return false;
		}
		return true;
	}
}
