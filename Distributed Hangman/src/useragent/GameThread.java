package useragent;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import common.ConnectionInfo;
import common.HangmanGameListItem;

/* Classe base per i thread di master e guesser. Si occupa della seconda fase di inizializzazione
 * della connessione del gioco e dell'invio sul gruppo multicast. Mantiene lo stato della partita
 * e gestisce la chiusura. */
public abstract class GameThread extends Thread{
	private GameDriver driver;
	private Socket sock;	
	private InetAddress ipinterface;	
	
	private AtomicBoolean exit = new AtomicBoolean(false);
	private AtomicInteger status = new AtomicInteger(HangmanGameListItem.GAME_INITIALIZATION);
	
	public GameThread(Socket sock, InetAddress ipinterface) {		
		this.sock = sock;
		this.ipinterface = ipinterface;					
	}
	
	@Override
	public void run() {
		//Inizializzazione
		startGame();
		
		if(!exit.get())
			status.set(HangmanGameListItem.GAME_PLAYING);
		
		//Permetto anche al figlio di inizializzarsi prima dell'inizio del gioco.
		if(!exit.get())
			onStart();				
		
		//In gioco.
		while(!exit.get())
			play();				
		
		//Gioco terminato.		
		status.set(HangmanGameListItem.GAME_TERMINATED);
	}
	
	//Chiamato prima della chiusura del gioco.
	abstract void onClose();

	//Chiamato dopo l'inizializzazione del gioco.
	abstract void onStart();
	
	//Esegue un turno di gioco.
	abstract void play();
	
	private void startGame() {
		JSONObject jo;
		try {
			//Attendo ConnectionInfo per al pi� 60 secondi.
			sock.setSoTimeout(60000);
			InputStreamReader input = new InputStreamReader(sock.getInputStream());
			char[] buf = new char[256];
			int len = input.read(buf, 0, 256);
			if(len==-1) {
				//Non ho letto niente. Chiudo il gioco.
				closeGame();
				return;
			}
			String s = new String(buf, 0, len);
			jo = (JSONObject) JSONValue.parse( s );
			ConnectionInfo conn;
			if(ConnectionInfo.validateJSON(jo))
				conn = new ConnectionInfo(jo);
			else {
				//JSON ricevuto non valido. Errore.
				closeGame();
				return;
			}
			driver = new GameDriver(InetAddress.getByName(conn.address), conn.port, ipinterface, conn.password);
			sock.close();
		} catch(SocketTimeoutException  e) {
			//Timeout. Esco dal gioco.
			System.out.println("Non � stato raggiunto il numero minimo di giocatori. Partita annullata.\n");			
			closeGame();
			return;
		} catch (IOException e) {
			//Errore di connessione generico.
			System.out.println("Errore di connessione: abbandono partita\n");
			closeGame();
			return;
		}
		
		System.out.println("Partita iniziata!");		
	}
	
	public int getStatus() { return status.get(); }
	
	public GameDriver getDriver() { return driver; }
	
	public boolean closed() { return exit.get(); }
	
	public synchronized void closeGame() {
		exit.set(true);
		onClose();
		status.set(HangmanGameListItem.GAME_TERMINATED);		 
		//La chiusura dei socket oltre a liberare le risorse interrompe eventuali
		//chiamate bloccanti su di essi.
		if(sock!=null) {
			try {
				sock.close();
			} catch(IOException e) {}
		}
		if(driver!=null) {
			driver.close();
		}
	}
}
