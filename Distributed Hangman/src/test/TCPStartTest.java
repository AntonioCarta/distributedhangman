package test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import useragent.GuesserThread;
import common.*;

//test effettuato con netcat in ascolto che si comporta da registry
public class TCPStartTest {
	public static void main(String[] args) throws IOException {
		String addr = "192.168.5.2";
		System.out.println(
			new ConnectionInfo(addr, 4242, "password")
				.toJSON().toJSONString()
		);
		
		Socket sock = new Socket(InetAddress.getByName("192.168.5.2"), 4242);
		Player p = new Player("andasda", "adadaa");
		
		System.out.println("starting guesser");
		GuesserThread guesser = new GuesserThread(sock, InetAddress.getByName("192.168.5.1"));
		guesser.start();
	}
}
