package registry;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class BuilderRegistry {

	public static void main(String[] args) {
		String configFile;
		String IPInterface;
		int RMIPort;
		String RMIServerName;
		int gameListLength;
		int gameServerPort;
		
		if(args.length != 1) {
			System.out.println("inserire nome del file di configurazione JSON:");			
			try {
				configFile = new BufferedReader( new InputStreamReader(System.in) ).readLine();
			} catch (IOException e) {
				System.out.println("Impossibile leggere l'input.");
				e.printStackTrace();
				return;
			}
		} else {
			configFile = args[0];
		}
		try {
			//leggo configurazioni JSON.
			JSONObject jo = (JSONObject) JSONValue.parse(new FileReader(configFile));
			IPInterface = (String) jo.get("IPInterface");
			RMIPort = ((Number) jo.get("RMIPort")).intValue();
			RMIServerName = (String) jo.get("RMIServerName");
			gameListLength = ((Number) jo.get("gameListLength")).intValue();
			gameServerPort = ((Number) jo.get("gameServerPort")).intValue();
		} catch (FileNotFoundException e2) {
			System.out.println("File di configurazione non trovato.");
			return;
		}
		
		System.setProperty("java.rmi.server.hostname", IPInterface);
		
		//Lancio il server lista partite.
		GameListServer gls = new GameListServer(gameListLength);
		gls.setDaemon(true);
		gls.start();
		
		RMIUserController objServer;
		try {
			objServer = new RMIUserController(gls);			
		} catch (RemoteException e1) {
			e1.printStackTrace();
			return;
		}
		
		Registry reg;
		
		try {
			//Lancio il server RMI.
			reg = LocateRegistry.createRegistry(RMIPort);
		} catch (RemoteException e) {
			System.out.println("Impossibile avviare la regystri RMI. Forse � gi� presente un server in esecuzione?");
			e.printStackTrace();
			return;
		}
		
		try {
			//Lancio il server utenti.
			reg.rebind(RMIServerName, objServer);
			System.out.println("Server utenti in esecuzione.");
		} catch (RemoteException e) {
			System.out.println("Impossibile avviare il server utenti");
			e.printStackTrace();
			return;
		}
		
		//Lancio il game server.
		(new GameServer(gameListLength, gameServerPort, gls)).start();
		System.out.println("Server partite in esecuzione.");
	}

}
