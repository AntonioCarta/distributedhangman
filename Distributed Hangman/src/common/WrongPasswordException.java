package common;

public class WrongPasswordException extends Exception {
	private static final long serialVersionUID = 710232214766239636L;
	
	public WrongPasswordException(String s) {
		super(s);
	}
}
