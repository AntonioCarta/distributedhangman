package test;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import registry.RMIUserController;
import common.IRegistryRMI;
import common.WrongPasswordException;

public class RMIuserTest {
	//test user RMI in locale
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, WrongPasswordException {
		//System.setProperty("java.rmi.server.hostname", args[1]);
		
		RMIUserController objServer;
		objServer = new RMIUserController(null);			
		
		Registry reg;
		reg = LocateRegistry.createRegistry(1111);
		reg.rebind("MyServer", objServer);
		System.out.println("Server utenti in esecuzione.");
		
		IRegistryRMI userServer;		
		userServer = (IRegistryRMI) Naming.lookup("rmi://127.0.0.1:1111/MyServer");		
		System.out.println("client in esecuzione.");
		
		userServer.login("marco", "tralalala");
		userServer.login("aldo", "password");		
		userServer.logout("marco");
		userServer.login("marco", "tralalala");
	}
}
