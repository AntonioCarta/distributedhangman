package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IGameList extends Remote {
	//inizializza la lista con nGame partite
	public void initList(String jsongames) throws RemoteException;
	
	//invia un update sul gioco
	public void upDateGame(String JSONGame) throws RemoteException;
}
