package registry;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

import common.IGameList;
import common.IRegistryRMI;
import common.Player;
import common.WrongPasswordException;

public class RMIUserController extends UnicastRemoteObject implements IRegistryRMI {
	private static final long serialVersionUID = 4384120567355017295L;
	
	HashMap<String, Player> db = new HashMap<String, Player>();
	GameListServer gls;
		
	public RMIUserController(GameListServer gls) throws RemoteException {
		super();
		this.gls = gls;
	}

	@Override
	public int login(String user, String password) throws RemoteException,
			WrongPasswordException {
		if(!db.containsKey(user)) {
			db.put(user, new Player(user, password));
			System.out.println("USER: " + user + " registrato e connesso.");
			return 1;
		}
		//utente � gi� registrato
		Player p = db.get(user);
		if(p.getPassword().equals(password)) {
			p.playerStatus = Player.PLAYER_CONNECTED;
			System.out.println("USER: " + user + " connesso.");
			return 1;
		} else if(!p.getPassword().equals(password)) {
			throw new WrongPasswordException("password errata");
		}
		
		return 0;
	}

	@Override
	public void logout(String user) throws RemoteException {
		if(db.containsKey(user)) {
			Player p = db.get(user);
			p.playerStatus = Player.PLAYER_DISCONNECTED;
			System.out.println("USER: " + user + " disconnesso.");
		}		
	}

	@Override
	public void register(Object callback) throws RemoteException {
		//ignoro errore di cast
		try{
			gls.addCallback( (IGameList) callback );
		} catch(Exception e) {}
	}

	@Override
	public void unregister(Object callback) throws RemoteException {
		try {
			gls.removeCallback( (IGameList) callback );
		} catch(Exception e) { /*Ignoro errori di cast*/ }
	}
}
