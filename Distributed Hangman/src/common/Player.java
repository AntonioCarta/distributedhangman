package common;

public class Player {
	private String nickname = "";
	private String password = "";
	
	public int playerStatus = 0;
	
	public static int PLAYER_DISCONNECTED = 0;
	public static int PLAYER_CONNECTED = 1;
	
	public Player(String nickname, String password) {
		this.nickname = nickname;
		this.password = password;
	}
	
	public String getNickname() { return nickname; }
	public String getPassword() { return password; }
}
