package registry;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import common.HangmanGameListItem;
import common.IGameList;

public class GameListServer extends Thread {
	private List<IGameList> clients;
	private BlockingQueue<JSONObject> msgs;
		
	//lista dei JSONObject rappresentanti la lista di giochi.
	List<JSONObject> list;
	
	public GameListServer(int gameNum) {
		clients = new ArrayList<IGameList>();
		msgs = new ArrayBlockingQueue<JSONObject>(100);
		list = new ArrayList<>(); 
		for(int i=0; i<gameNum; i++)
			list.add((new HangmanGameListItem(i)).toJSON());
	}
	
	@Override
	public void run() {
		List<IGameList> toelim = new ArrayList<>();
		while(true) {			
			try {
				JSONObject jo = msgs.take();				
				int index = ((Number) jo.get("gameid")).intValue();
				list.set(index, jo);
				for(IGameList gl : clients) {					
					try {
						gl.upDateGame(jo.toJSONString());
					} catch (RemoteException e) {
						// Devo eliminare gl dalla lista dei clients
						// ma non posso modificare la lista dentro il for
						toelim.add(gl);
					}
				}
				
				clients.removeAll(toelim);
				toelim.clear();
			} catch (InterruptedException e) {
				//e.printStackTrace();
				System.out.println("gamelistserver interrotto: " + e);
			}
		}
	}

	public void addCallback(IGameList callback) {
		try {
			callback.initList(JSONValue.toJSONString(list));
			//lo aggiungo solo se risponde correttamente all'init
			clients.add(callback);			
			System.out.println("utente collegato al servizio partite");
		} catch(RemoteException e) {
			;//se fallisco non lo aggiungo tra i client da aggiornare
		}
	}
	
	//aggiunge il messaggio alla coda dei messaggi da spedire
	//e aggiorna la lista interna dei giochi
	public void addMessage(JSONObject jo) {		
		int i = ((Number) jo.get("gameid")).intValue();
		msgs.add(jo);
		list.set(i, jo);
	}
	
	//usato per deregistrarsi dal servizio di lista partite, ad esempio mentre si gioca.
	public void removeCallback(IGameList callback) {
		clients.remove(callback);
	}
}
