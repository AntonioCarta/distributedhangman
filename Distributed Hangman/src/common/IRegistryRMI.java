package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IRegistryRMI extends Remote {
	//se non esiste registra il nuovo utente.
	//Restituisce userid
	public int login(String user, String password) throws RemoteException, WrongPasswordException;

	//se user non � loggata l'operazione non fa nulla
	public void logout(String user) throws RemoteException;
	
	//registra al servizio di lista partite
	public void register(Object callback) throws RemoteException;
	
	//registra al servizio di lista partite
	public void unregister(Object callback) throws RemoteException;
}
