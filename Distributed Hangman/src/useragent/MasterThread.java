package useragent;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class MasterThread extends GameThread {	
	GameDriver driver;
	
	//game state
	String solution;
	String partialSolution;
	char lastGuess;
	int nerr = 0;	
	static final int MAXERR = 10;
	
	public MasterThread(String solution, Socket sock, InetAddress ipinterface) throws IOException {		
		super(sock, ipinterface);
		
		this.solution = solution;
		StringBuffer outputBuffer = new StringBuffer(solution.length());
		for (int i = 0; i < solution.length(); i++){
		   outputBuffer.append("*");
		}
		this.partialSolution = outputBuffer.toString();
	}
	
	@Override
	void onStart() {
		driver = getDriver();		
		driver.sendGameState(lastGuess, partialSolution, nerr, getStatus(), false);
	}
	
	@Override
	void onClose() {
		//Avviso i guesser dell'abbandono.
		if(driver!=null)
			driver.sendGameState(lastGuess, partialSolution, nerr, getStatus(), true);
	}
	
	@Override
	void play() {			
		char guess = driver.readGuess();
		if(guess == '*') {
			closeGame();
			return;
		}
		processGuess(guess);
		driver.sendGameState(lastGuess, partialSolution, nerr, getStatus(), false);
		printGameState();						
	}
	

	private void printGameState() {
		System.out.println(
				"Soluzione parziale: " + partialSolution + 
				"\nerrori: " + nerr + 
				"\nUltimo indizio: " + lastGuess + "\n\n"
		);
	}
	
	/* Aggiorno lo stato e verifico se la partita � conclusa. */
	private void processGuess(char guess) {
		this.lastGuess = guess;
		boolean rightGuess = false;
		for(int i=0; i<solution.length(); i++) {
			char c = solution.charAt(i);
			if(c == guess) {
				rightGuess = true;
				partialSolution = partialSolution.substring(0, i) + c + partialSolution.substring(i+1);		
			}
		}
		
		//verifico se la partita � terminata
		if(rightGuess) {				
			if(solution.equals(partialSolution)) {
				//vittoria guesser
				System.out.println("Partita completata. Vincono i guesser!");				
				closeGame();
			}
		} else {
			nerr++;
			if(nerr >= MAXERR) {
				//vittoria master
				System.out.println("Partita terminata. Vittoria!");
				closeGame();
			}
		}
	}
}
