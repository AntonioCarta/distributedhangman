package common;

import java.util.concurrent.atomic.AtomicInteger;



import org.json.simple.JSONObject;

//Gli elementi della lista di partite
public class HangmanGameListItem {
	private String masterName;
	private int currPlayer = 0;
	private int maxPlayer = 10;
	
	public int gameid; //usato per identificare il gioco con la registry
	
	private AtomicInteger status = new AtomicInteger(GAME_TERMINATED);
	
	public static final int GAME_INITIALIZATION = 0;
	public static final int GAME_PLAYING = 1;
	public static final int GAME_TERMINATED = 2;
	
	public HangmanGameListItem(int gameid) {
		this.gameid = gameid;
	}
	
	public HangmanGameListItem(JSONObject jo) {
		this.gameid = ((Number) jo.get("gameid")).intValue();
		this.masterName = (String) jo.get("masterName");
		this.currPlayer = ((Number) jo.get("currPlayer")).intValue();
		this.maxPlayer = ((Number) jo.get("maxPlayer")).intValue();
		this.status.set( ((Number) jo.get("status")).intValue() );
	}
	
	public String getMasterName() {
		return masterName;
	}

	public synchronized int getCurrPlayer() {
		return currPlayer;
	}

	public int getMaxPlayer() {
		return maxPlayer;
	}

	public HangmanGameListItem(String master, int id) {
		this.masterName = master;
		this.gameid = id;
	}
	
	public synchronized boolean addPlayer() {
		if(currPlayer < maxPlayer) {
			currPlayer++;
			return true;
		}
		return false;
	}
	
	public synchronized boolean removePlayer() {
		if(currPlayer > 0) {
			currPlayer--;
			return true;
		}
		return false;
	}
	
	public void setMaxPlayer(int m) { this.maxPlayer = m; }
	
	public void setStatus(int newStatus) {
		status.set(newStatus);
	}
	
	public int getStatus() { return status.get(); }
	
	public synchronized void setNewGame(int maxPlayer, String mastername) {
		this.maxPlayer = maxPlayer;
		this.masterName = mastername;
		this.status.set(HangmanGameListItem.GAME_INITIALIZATION);
		this.currPlayer = 0;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject jo = new JSONObject();
		jo.put("gameid", this.gameid);
		jo.put("masterName", this.masterName);
		jo.put("currPlayer", this.currPlayer);
		jo.put("maxPlayer", this.maxPlayer);
		jo.put("status", this.status.get());
		return jo;
	}

	/* Verifica se il JSONObject passato � una rappresentazione corretta di un oggetto HangmanGame */
	@SuppressWarnings("unused")
	public static boolean validateJSON(JSONObject jo) {				
		if(!jo.containsKey("gameid") || !jo.containsKey("masterName") || !jo.containsKey("currPlayer") ||
				!jo.containsKey("maxPlayer") || !jo.containsKey("status"))
			return false;
		try {
			int gameid = ((Number) jo.get("gameid")).intValue();
			String masterName = (String) jo.get("masterName");
			int currPlayer = ((Number) jo.get("currPlayer")).intValue();
			int maxPlayer = ((Number) jo.get("maxPlayer")).intValue();
			int status = ((Number) jo.get("status")).intValue();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public String prettyString() {
		String s = "";
		if(getStatus() == GAME_INITIALIZATION)
			s = "init";
		else if(getStatus() == GAME_PLAYING)
			s = "game";
		else if(getStatus() == GAME_TERMINATED)
			s = "end ";
		return s + " " + gameid + ": " + this.currPlayer + "/" + this.maxPlayer + " (" + this.masterName + ")";
	}
	
	@Override
	public String toString() {
		return this.toJSON().toJSONString();
	}
}
