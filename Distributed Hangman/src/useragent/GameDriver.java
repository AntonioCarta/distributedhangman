package useragent;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jasypt.util.text.*;

/* Questa classe si occupa dell'invio e la ricezione dei messaggi di gioco tramite 
 * multicast UDP. Effettua la crittografia/decrittografia dei pacchetti e valida i JSON
 * ricevuti in ingresso.
 * Pu� spedire due tipi di messaggi:
 *  - guess: 
 *  	� il messaggio inviato dai guesser e rappresenta un guess
 *  - gameState:
 *  	� il messaggio inviato dal master e rappresenta lo stato attuale
 *  	del gioco. Contiene inoltre l'ultimo guess ricevuto (una sorta di
 *  	ACK per l'utente). 
 *  */
public class GameDriver {
	MulticastSocket sock;
	InetAddress group;
	int port;
	
	JSONParser parser = new JSONParser();
	BasicTextEncryptor enc;
	
	//Usati per verificare la ricezione dell'ACK
	boolean ACKreceived = true;
	char lastSent = '*';
	
	public static final String CLOSE_GAME = "closeGame";
	public static final String LAST_GUESS = "lastGuess";
	public static final String IS_GUESS = "isGuess";
	public static final String CURR_SOL = "currSol";
	public static final String NERR = "nerr";
	public static final String STATUS = "status";
	
	
	public GameDriver(InetAddress ip, int port, InetAddress ipinterface, String password) throws IOException {
		sock = new MulticastSocket(port);
		sock.setInterface(ipinterface);
		sock.setSoTimeout(60000);
		sock.joinGroup(ip);
		this.group = ip;
		this.port = port;
		
		enc = new BasicTextEncryptor();
		enc.setPassword(password);
	}
	
	@SuppressWarnings("unchecked")
	public void sendGuess(char c) {
		ACKreceived = false;
		lastSent = c;
		
		JSONObject jo = new JSONObject(); 
		jo.put( LAST_GUESS, "" + c);
		jo.put(IS_GUESS, true);

		sendGameMessage(jo);
	}
	
	@SuppressWarnings("unchecked")
	public void sendGameState(char lastGuess, String currSol, int nerr, int status, boolean closeGame) {
		JSONObject jo = new JSONObject();
		jo.put( LAST_GUESS, "" + lastGuess);
		jo.put( CURR_SOL, currSol);
		jo.put(NERR, nerr);
		jo.put(IS_GUESS, false);
		jo.put(STATUS, status);
		jo.put(CLOSE_GAME, closeGame);
		
		sendGameMessage(jo);
	}
	
	private void sendGameMessage(JSONObject jo) {
		/* NOTA: l'operazione di crittografia del messaggio pu� impiegare un tempo molto lungo 
		 * sulle macchine virtuali. Questo � causato dal fatto che jasypt usa sorgenti di bit casuali
		 * (/dev/random) che vengono esaurite e quindi si blocca in attesa dell'arrivo di nuovi bit. */
		String s = enc.encrypt(jo.toJSONString());
		byte[] buf = s.getBytes();
		DatagramPacket pac = new DatagramPacket(buf, buf.length, group, port);		
		//System.out.println("provo ad inviare: " + jo.toJSONString());
		try {
			sock.send(pac);
		} catch (IOException e) {
			System.err.println("impossibile inviare guess");
		}
	}
	
	//ciclo finch� non mi arriva un messaggio corretto
	public char readGuess() {
		JSONObject jo = readMessage();		
		
		//errore connessione. Ritorno un valore impossibile.
		if(jo==null)
			return '*';
		
		if(jo.containsKey(IS_GUESS) && jo.containsKey(LAST_GUESS) && (boolean) jo.get(IS_GUESS)) {
			char s;
			try {
				s = ((String) jo.get(LAST_GUESS)).charAt(0);
			} catch(Exception e) {
				//cast errato
				return readGuess();
			}
			return s;
		} else {
			//se non era un guess ignoro e leggo il successivo
			//anche se era un msg corretto.
			//Questo va bene perch� ogni giocatore legge un solo tipo di messaggi.			
			return readGuess();
		}
	}
	
	//return null in caso di fallimento
	public JSONObject readGameState() {
		JSONObject msg = readMessage();
		
		//errore connessione.
		if(msg == null)
			return msg;
		
		//controllo validit� messaggio
		if(msg.containsKey(IS_GUESS) && msg.containsKey(LAST_GUESS) && msg.containsKey(NERR)
				&& msg.containsKey(STATUS) && msg.containsKey(CURR_SOL) && msg.containsKey(CLOSE_GAME)
				&& !((boolean) msg.get(IS_GUESS))) {
			
			if(!ACKreceived) {
				char c = ((String)msg.get(LAST_GUESS)).charAt(0);
				if(c==lastSent)
					ACKreceived = true;
				else
					sendGuess(lastSent);
			}
			
			return msg;
		} else {
			//se non era un guess ignoro e leggo il successivo
			//anche se era un msg corretto.
			//Questo va bene perch� ogni giocatore legge un solo tipo di messaggi
			//quindi non scarto messaggi utili
			return readGameState();
		}
	}

	public JSONObject readMessage() {
		byte[] buf = new byte[1024];
		DatagramPacket p = new DatagramPacket(buf, buf.length, group, port);
		
		try {
			sock.receive(p);			
		} catch (IOException e) {
			System.err.println("impossibile ricevere guess");
			return null;
		}
		String s = new String(buf, 0, p.getLength());
		s = enc.decrypt(s);
		JSONObject jo;
		try {
			jo = (JSONObject) parser.parse(s);
		} catch (ParseException e) {
			// ho ricevuto schifezze. Ignoro il pacchetto e leggo il successivo
			return readMessage();			
		}
		return jo;
	}

	public void close() {
		if(sock != null) {
			try {
				sock.leaveGroup(group);
			} catch (IOException e) { }
			sock.close();
		}
	}
}

