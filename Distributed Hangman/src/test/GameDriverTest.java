package test;

import java.io.IOException;
import java.net.InetAddress;
import common.HangmanGameListItem;

import useragent.*;

//Test per la comunicazione tra master e guesser
public class GameDriverTest {
	public static void main(String[] args) throws IOException {
		InetAddress group = InetAddress.getByName("230.0.0.1");
		int port = 4230;
		
		GameDriver guesser = new GameDriver(group, port, InetAddress.getByName("192.168.5.1"), "asdasd");
		GameDriver master = new GameDriver(group, port, InetAddress.getByName("192.168.5.1"),  "asdasd");
		
		guesser.sendGuess('a');
		
		master.sendGameState('c', "a_d_q", 3, HangmanGameListItem.GAME_PLAYING, false);
		
		System.out.println("messaggi inviati");
		
		System.out.println( master.readGuess() );
		
		System.out.println( guesser.readGameState() );
	}
}
