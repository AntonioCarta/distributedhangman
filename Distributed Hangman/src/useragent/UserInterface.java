package useragent;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

import common.HangmanGameListItem;
import common.IRegistryRMI;
import common.Player;
import common.WrongPasswordException;

public class UserInterface extends Thread{
	IRegistryRMI rmidriver;
	GameListClient gamelist;
	Player currPlayer;
	GameThread gameThread;
	boolean isMaster;
	
	InetAddress ipinterface;
	InetAddress serverAddress;
	int serverPort = 1234;
	BufferedReader input = new BufferedReader( new InputStreamReader(System.in) );
	
	public UserInterface(IRegistryRMI rmi, GameListClient glc, InetAddress ipinterface, String serverName) throws UnknownHostException, RemoteException {
		super();		
		this.rmidriver = rmi;	
		this.gamelist = glc;
		this.serverAddress = InetAddress.getByName(serverName);		
		this.ipinterface = ipinterface;		
	}
	
	@Override
	public void run() {
		boolean exit = false;
		while(!exit) {
			printCommandList();
			String c = readCommand();
			exit = processCommand(c);
		}
		if(gameThread != null)
			gameThread.closeGame();
		
		try {
			if(currPlayer!=null) {
				rmidriver.logout(currPlayer.getNickname());
			}
			rmidriver.unregister(gamelist);
		} catch (RemoteException e) { }
		
		System.out.println("Gioco terminato.");
		System.exit(0);
	}
	
	private void printCommandList() {
		if(currPlayer == null) {
			//Disconnesso. Devo loggarmi.
			System.out.println("Bentornato! Esegui il login per iniziare una partita.\nuser:");
		}else if(gameThread == null) {
			//Connesso. Devo unirmi ad una partita.
			gamelist.printList();
			if(!gamelist.isEmpty())
				System.out.println("Inserire l'indice della partita a cui si desidera partecipare: ");			
		} else if(gameThread.getStatus() == HangmanGameListItem.GAME_PLAYING && isMaster) {
			//In gioco. Sto giocando e sono il master.
			System.out.println("In attesa dei guess. (Digitare 'exit' per uscire)");
		} else if(gameThread.getStatus() == HangmanGameListItem.GAME_PLAYING && !isMaster) {
			//In gioco. Sto giocando e sono guesser
			System.out.println("Inserire guess da inviare:");
		} else if(gameThread.getStatus() == HangmanGameListItem.GAME_INITIALIZATION) {
			//Attesa partita. In attesa degli altri giocatori
			System.out.println("In attesa degli altri giocatori. (Premere 'exit' per abbandonare)");
		} else if(gameThread.closed()) {
			//Gioco terminato. Bisogna annullarlo
			gameThread = null;
		} 
	}

	public String readCommand() {		
		try {
			return input.readLine();		
		} catch (IOException e) {
			// Questo non dovrebbe succedere. Non posso leggere l'input dell'utente.
			// Chiudo il processo.
			e.printStackTrace();
			System.exit(-1);
		}
		return "";
	}

	private boolean processCommand(String c) {
		if(currPlayer == null) {
			//Disconnesso. Devo loggarmi.
			loginPlayer(c);
		}else if(gameThread == null) {
			//Connesso. Devo unirmi ad una partita.
			if(!gamelist.isEmpty()) {
				try {
					int i = Integer.parseInt(c);
					joinGame(i);
				} catch(NumberFormatException e) {
					System.out.println("Inserire l'indice di una partita per iniziare a giocare.");
				}				
			}
			if(c.equals("exit")) 
				return true;
		} else if(gameThread.getStatus() == HangmanGameListItem.GAME_PLAYING && c.equals("exit")) {
			//In gioco. Voglio chiudere la partita.
			gameThread.closeGame();
		} else if(gameThread.getStatus() == HangmanGameListItem.GAME_PLAYING && !isMaster) {
			//In gioco. Sto giocando e sono guesser
			if(c.length() == 1) {
				System.out.println("invio guess: " + c.charAt(0));
				((GuesserThread) gameThread).sendGuess(c.charAt(0)); 
			}
		} else if(gameThread.getStatus() == HangmanGameListItem.GAME_INITIALIZATION) {
			//Attesa partita. In attesa degli altri giocatori
			if(c.equals("exit")) gameThread.closeGame();			
		} else if(gameThread.closed()) {		
			//gioco terminato. Bisogna annullarlo
			gameThread = null;
		} 
		return false;		
	}
	
	/* Prima fase di inizializzazione della partita. */
	private void joinGame(int i) {
		HangmanGameListItem g = gamelist.getGame(i);
		if(g == null) {
			System.out.println("Indice non valido.");
			return;
		}
		
		Socket gameSocket=null;		
		try {			
			gameSocket = new Socket(serverAddress, serverPort);
			
			DataOutputStream out = new DataOutputStream(gameSocket.getOutputStream());				
			out.writeInt(i); //gameid
			
			int x = gameSocket.getInputStream().read();
			isMaster = (x==1 ? true : false);
			if(isMaster) {
				//sei un master			
				System.out.println("Sei il master. Scegli la parola segreta.");
				String sol;
				sol = input.readLine();	
				int x1 = getPlayerNumber();
				out.writeInt(x1); //TODO maxp
				out.writeUTF(currPlayer.getNickname()); //master name

				gameThread = new MasterThread(sol, gameSocket, ipinterface);						
			} else {
				//sei un guesser			
				System.out.println("Sei un guesser.");
				gameThread = new GuesserThread(gameSocket, ipinterface);					
			}
			gameThread.start();
			System.out.println("Attendere gli altri giocatori...");
		} catch (IOException e1) {
			System.out.println("Impossibile inizializzare la partita. Scegliere un'altra partita.");
			gameThread = null;
			try {
				if(gameSocket != null)
					gameSocket.close();
			} catch(IOException e) {}
			return; 
		}
	}

	/* leggo un numero positivo dall'utente da usare come numero di giocatori per la partita. */
	private int getPlayerNumber() {
		while(true) {
			System.out.println("Inserire il numero di giocatori: ");
			String c = readCommand();
			try {
				int i = Integer.parseInt(c);
				if(i>1)
					return i;
			} catch(NumberFormatException e) {
				System.out.println("Inserire un numero positivo.");
			}	
		}
	}

	private void loginPlayer(String user) {
		System.out.println("password:");
		try {
			String p = input.readLine();
			rmidriver.login(user, p);
			currPlayer = new Player(user, p);
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Impossibile contattare il server utenti: " + e);
			System.exit(-1);
		} catch (WrongPasswordException e) {
			System.out.println("Password errata.");
			currPlayer = null;
		}
	}
}
