package useragent;

import java.net.InetAddress;
import java.net.Socket;
import org.json.simple.JSONObject;

public class GuesserThread extends GameThread {
	GameDriver driver;	
	
	//game state
	String partialSolution;
	char lastGuess = '*';
	int nerr = 0;	
	static final int MAXERR = 10;
	
	public GuesserThread(Socket sock, InetAddress ipinterface) {
		super(sock, ipinterface);
	}
	
	@Override
	void onStart() {
		driver = getDriver();
		JSONObject jo = driver.readGameState();
		if( ((boolean) jo.get(GameDriver.CLOSE_GAME)) ) 
			closeGame();
		String s = (String) jo.get(GameDriver.CURR_SOL);
		partialSolution = s;
		System.out.println("Soluzione parziale: " + partialSolution);
	}
	
	@Override
	void play() {
		JSONObject jo = driver.readGameState();
		if(jo==null) {
			closeGame();
			return;
		}
		processState(jo);
		printGameState();				
	}	

	private void printGameState() {
		System.out.println(
				"soluzione parziale: " + partialSolution + 
				"\nerrori: : " + nerr +"\n" +
				"ultimo tentativo: " + lastGuess + "\n"
		);
	}

	/* Aggiorno lo stato e verifico se la partita � conclusa. */
	private void processState(JSONObject jo) {
		if( ((boolean) jo.get(GameDriver.CLOSE_GAME)) ) {
			System.out.println("Il master ha chiuso la partita.");
			closeGame();						
		}
		
		partialSolution = (String) jo.get(GameDriver.CURR_SOL);
		nerr = ((Number) jo.get(GameDriver.NERR)).intValue();
		lastGuess = ((String) jo.get(GameDriver.LAST_GUESS)).charAt(0);
		
		if(nerr == MAXERR) {
			//vittoria master
			System.out.println("Partita terminata. Vince il master!");
			closeGame();
		}
		else if ( !partialSolution.contains("*") ) {
			// vittoria guesser
			System.out.println("Partita terminata. Vittoria!");
			closeGame();
		}
	}
	
	public void sendGuess(char c) {
		driver.sendGuess(c);
	}

	@Override
	void onClose() { }
}
