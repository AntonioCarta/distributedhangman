package test;

import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import registry.GameListServer;
import registry.GameServer;
import common.ConnectionInfo;

public class GuesserMasterStub {
	public static void main(String[] args) throws InterruptedException {
		GameListServer gls = new GameListServer(10);
		
		(new GameServer(5, 1234, gls)).start();
		System.out.println("server avviato");
		
		(new Thread() {
			public void run() {
				try {
				InetAddress ip = InetAddress.getByName("192.168.5.1");
				int port = 1234;
				Socket so = new Socket(ip, port);
				
				DataOutputStream out = new DataOutputStream(so.getOutputStream());
				out.writeInt(2); //gameid
				out.writeInt(2); //num giocatori
				out.writeUTF("master");
				System.out.println("MASTER: inviati gameid e num. giocatori");
				
				InputStreamReader input = new InputStreamReader(so.getInputStream());
				input.read(); //isMaster
				char[] buf = new char[256];
				int len = input.read(buf, 0, 256);
				String s = new String(buf, 0, len);
				JSONObject jo = (JSONObject) JSONValue.parse( s );
				ConnectionInfo conn  = new ConnectionInfo(jo);
				System.out.println(conn.toJSON().toJSONString());
				System.out.println("MASTER: game started");
				
				so.close();
				System.out.println("MASTER: game closed");
				
				} catch(Exception e) {System.out.println("MASTER: errore"); }
			};
		}).start();
		System.out.println("master stub avviato");
		Thread.sleep(2000);
		
		(new Thread() {
			public void run() {
				try {
					InetAddress ip = InetAddress.getByName("192.168.5.1");
					int port = 1234;
					Socket so = new Socket(ip, port);
					
					DataOutputStream out = new DataOutputStream(so.getOutputStream());
					out.writeInt(2); //gameid
					System.out.println("GUESSER: inviato gameid");
					
					InputStreamReader input = new InputStreamReader(so.getInputStream());
					input.read(); //isMaster
					char[] buf = new char[256];
					int len = input.read(buf, 0, 256);
					String s = new String(buf, 0, len);
					JSONObject jo = (JSONObject) JSONValue.parse( s );
					ConnectionInfo conn  = new ConnectionInfo(jo);
					System.out.println(conn.toJSON().toJSONString());
					
					System.out.println("GUESSER: game started");
							
					Thread.sleep(3000);
					if(so.isClosed()) 
						System.out.println("GUESSER: game closed");
				} catch(Exception e) {System.out.println("GUESSER: errore"); }
			};
		}).start();
		System.out.println("guesser stub avviato");
	}
}
