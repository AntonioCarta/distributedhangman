package registry;

import java.io.IOException;
import java.net.Socket;

import common.ConnectionInfo;
import common.HangmanGameListItem;

public class GameInfo {
	private HangmanGameListItem game = null;
	public ConnectionInfo conn;
	private Socket[] socketArray;
	private boolean hasMaster = false;
	private GameListServer gls;
	
	private Socket masterSocket;
	
	public GameInfo(ConnectionInfo conn, int id, GameListServer gls) {
		game = new HangmanGameListItem(id); //creiamo una partita vuota con 10 giocatori max		
		this.conn = conn;
		this.gls = gls;
	}
	
	/* return player id oppure -1 se la partita � completa. */
	public synchronized int addPlayer(Socket so) {
		int index = -1;
		if(game.getStatus() != HangmanGameListItem.GAME_INITIALIZATION)
			return -1;
		for(int i=1; i<socketArray.length; i++) {
			if(socketArray[i] == null) {
				game.addPlayer();
				socketArray[i] = so;				
				index = i;
				break;
			}				
		}
		gls.addMessage(game.toJSON());
		
		//Si � unito l'ultimo giocatore
		if(index != -1 && game.getCurrPlayer() == game.getMaxPlayer())
			startGame();
			
		return index;
	}
	
	public synchronized void removePlayer(int id) {
		socketArray[id] = null;
		game.removePlayer();
		gls.addMessage(game.toJSON());
	} 
	
	/* Inizializzo un nuovo gioco. */
	public synchronized void setNewGame(int maxp, String master) {		
		this.game.setNewGame(maxp, master);
		this.socketArray = new Socket[maxp];
		socketArray[0] = masterSocket;
		masterSocket = null;
		game.addPlayer();
		gls.addMessage(game.toJSON());
		game.setStatus(HangmanGameListItem.GAME_INITIALIZATION);
		System.out.println("GAME: partita di " + game.getMasterName() + " creata.");
	}
		
	/* Chiudo tutti i socket e aggiorno lo stato della partita. */
	public synchronized void closeGame() {
		try {
			if(masterSocket != null) masterSocket.close();
		} catch(IOException e) {}
		if(socketArray!=null) {
			for(int i=0; i<socketArray.length; i++) {			
				try {
					if(socketArray[i] != null)
						socketArray[i].close();
				} catch(IOException e) {}
				socketArray[i] = null;
			}
		}		
		if(game!=null) game.setStatus(HangmanGameListItem.GAME_TERMINATED);
		hasMaster = false;
		gls.addMessage(game.toJSON());
		System.out.println("GAME: partita di " + game.getMasterName() + " chiusa.");
	}
	
	/* Inizio della partita. Invio ConnectionInfo a tutti i partecipanti e chiude i socket dei guesser. */
	public synchronized void startGame() {		
		try{
			if(socketArray[0] != null) {
				socketArray[0].getOutputStream().write(conn.toJSON().toJSONString().getBytes());
			}
		} catch(IOException e) {  }
		try {
			if(socketArray[0] != null)
				socketArray[0].shutdownOutput();
		} catch(IOException e) { closeGame(); }

		//invio ConnectionInfo ai guesser.
		for(int i=1; i<socketArray.length; i++) {
			try{
				if(socketArray[i] != null) {
					socketArray[i].getOutputStream().write(conn.toJSON().toJSONString().getBytes());
				}
			} catch(IOException e) {  }
			try {
				if(socketArray[i] != null)
					socketArray[i].close();
			} catch(IOException e) {}
			socketArray[i] = null;
		}
		game.setStatus(HangmanGameListItem.GAME_PLAYING);
		gls.addMessage(game.toJSON());
				
		System.out.println("GAME: partita di " + game.getMasterName() + " inizializzata.");
	}

	/* return true se il gioco non � stato ancora inizializzato e non ha un master.
	 * Pu� essere chiamata per assicurarsi di essere l'unico master della partita. */
	public synchronized boolean setMaster(Socket masterSocket) {
		this.masterSocket = masterSocket;
		if(hasMaster)
			return false;
		hasMaster = true;
		return true;
	}

	public synchronized int getStatus() {
		return game.getStatus();
	}

	public synchronized boolean hasMaxPlayer() {
		return game.getCurrPlayer() == game.getMaxPlayer();
	}
}
