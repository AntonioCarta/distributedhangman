package useragent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import common.IRegistryRMI;

public class BuilderUA {

	public static void main(String[] args) throws UnknownHostException {
		String configFile;
		String IPInterface;
		String serverName;
		int serverPort;
		String RMIServerName;
		
		if(args.length != 1) {
			System.out.println("inserire nome del file di configurazione JSON:");			
			try {
				configFile = new BufferedReader( new InputStreamReader(System.in) ).readLine();
			} catch (IOException e) {
				System.out.println("Impossibile leggere l'input.");
				e.printStackTrace();
				return;
			}
		} else {
			configFile = args[0];
		}
		try {
			JSONObject jo = (JSONObject) JSONValue.parse(new FileReader(configFile));
			IPInterface = (String) jo.get("IPInterface");
			serverName = (String) jo.get("serverName");
			serverPort = ((Number) jo.get("serverPort")).intValue();
			RMIServerName = (String) jo.get("RMIServerName");
		} catch (FileNotFoundException e2) {
			System.out.println("File di configurazione non trovato.");
			return;
		} catch(Exception e) {
			System.out.println("File di configurazione corrotto.");
			return;
		}
		
		System.setProperty("java.rmi.server.hostname", IPInterface); 
		
		IRegistryRMI userServer;
		try {
			userServer = (IRegistryRMI) Naming.lookup("rmi://" + serverName + ":" + serverPort + "/" + RMIServerName); 		
			GameListClient glc = new GameListClient();
			userServer.register(glc);
			System.out.println("Connessione al server inizializzata.");
			(new UserInterface(userServer, glc, InetAddress.getByName(IPInterface), serverName)).start();
		} catch (MalformedURLException e) {
			System.out.println("URL della registry errata. Correggere la configurazione.");
		} catch (RemoteException e) {
			System.out.println("Connessione con la registry fallita.");
		} catch (NotBoundException e) {
			System.out.println("Il server utenti della registry � disattivato. Impossibile accedere al servizio.");
		}						
	}

}
